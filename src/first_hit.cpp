#include "first_hit.h"

bool first_hit(
  const Ray & ray, 
  const double min_t,
  const std::vector< std::shared_ptr<Object> > & objects,
  int & hit_id, 
  double & t,
  Eigen::Vector3d & n)
{
  ////////////////////////////////////////////////////////////////////////////
  // Replace with your code here:

  //values to keep track of the result closest to the ray origin
  double smallest_t = std::numeric_limits<double>::infinity();
  Eigen::Vector3d smallest_n;
  bool hit = false;
  bool was_hit = false;

  //loop through all of the objects and check for intersection, take the closest intersecton
  for(int i=0; i < objects.size(); i++){
    hit = objects[i] -> intersect(ray, min_t, t, n);
    if (hit){
      was_hit = true;
      if(t < smallest_t){
        hit_id = i;
        smallest_t = t;
        smallest_n = n;
      }
    }
  }

  //If nothing was hit, return false
  if(!was_hit){
    return false;
  }

  //if something was hit, return the closest values
  if (t){
    t = smallest_t;
    n = smallest_n;
    return true;
  }
  return false;
  ////////////////////////////////////////////////////////////////////////////
}

