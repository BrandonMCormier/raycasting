#include "Plane.h"
#include "Ray.h"

bool Plane::intersect(
  const Ray & ray, const double min_t, double & t, Eigen::Vector3d & n) const
{
  ////////////////////////////////////////////////////////////////////////////
  // Replace with your code here:


  //If ray runs parallel to the plane, perpendicular to the plane normal, return false to avoid error
  if (n.dot(ray.direction) == 0){
    return false;
  }

  //plane normal
  n = normal;

  //check t
  double check = (point.dot(n) - n.dot(ray.origin))/(n.dot(ray.direction));
  
  if (check >= min_t){
    t = check;
    return true;
  }

  return false;
  ////////////////////////////////////////////////////////////////////////////
}


  